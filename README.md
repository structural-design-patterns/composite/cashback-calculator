# Cashback Calculator

### Description
An example of a Composite pattern. In this implementation Cart is a composite,\
Item is a leaf and Cashbackable is a common interface. 

Since this is the simplest version, every Item has a 1% cashback