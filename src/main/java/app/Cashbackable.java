package app;

public interface Cashbackable {
    double calculate();
}
