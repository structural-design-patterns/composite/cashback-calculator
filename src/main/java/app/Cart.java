package app;

import java.util.ArrayList;
import java.util.List;

public class Cart implements Cashbackable {
    private List<Cashbackable> items;

    public Cart() {
        this.items = new ArrayList<>();
    }

    public Cart(List<Cashbackable> items) {
        this.items = items;
    }

    public void add(Cashbackable item) {
        items.add(item);
    }

    public void remove(Cashbackable item) {
        items.remove(item);
    }

    @Override
    public double calculate() {
        return items.stream().mapToDouble(Cashbackable::calculate).sum();
    }
}
