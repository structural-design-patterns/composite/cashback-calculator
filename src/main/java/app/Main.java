package app;

import java.util.List;

public class Main {

    // A Client Code example
    public static void main(String[] args) {
        Cashbackable mainCart = init();

        double expectedCashback = mainCart.calculate();

        System.out.println(expectedCashback);
    }

    private static Cashbackable init() {
        List<Cashbackable> items = List.of(new Item(100), new Item(1_000));
        Cart nestedCart = new Cart(items);

        Cart mainCart = new Cart();
        mainCart.add(nestedCart);
        mainCart.add(new Item(10_000));

        return mainCart;
    }
}
