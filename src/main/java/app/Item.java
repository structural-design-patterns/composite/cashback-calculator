package app;

public class Item implements Cashbackable {
    private final double price;

    public Item(double price) {
        this.price = price;
    }

    @Override
    public double calculate() {
        return price * 0.01;
    }
}
